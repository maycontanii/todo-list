import axios from 'axios'
import {actionTypes, mutationTypes} from '@/commons/constants'

export default {

    async [actionTypes.ATUALIZAR_TAREFA](context, tarefa) {
        return await axios.put(`todo/${tarefa.id}`, tarefa)
    },

    async [actionTypes.BUSCAR_TAREFAS]({commit}) {
        const {data} = await axios.get('todo/')
        commit(mutationTypes.SET_AFAZERES, data)
    },

    async [actionTypes.INSERIR_TAREFA](context, tarefa) {
         return await axios.post('todo/', tarefa)
    },

    async [actionTypes.REMOVER_TAREFA](context, tarefaId) {
        return await axios.delete(`todo/${tarefaId}`)
    }

}
