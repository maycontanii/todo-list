import Vue from 'vue'
import {mutationTypes} from '../constants'

export default {

    [mutationTypes.SET_AFAZERES](state, novosAfazeres) {
        Vue.set(state, 'listaTarefas', novosAfazeres)
    }
}
