export default {
    ATUALIZAR_TAREFA: 'atualizarTarefa',
    BUSCAR_TAREFAS: 'buscarTarefas',
    INSERIR_TAREFA: 'inserirAfazer',
    REMOVER_TAREFA: 'removerAfazer'
}
